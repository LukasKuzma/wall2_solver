# wall1 Solver

Šioje repozitorijoje yra sprendžiamas wall2 programos uždavinys. Uždavinį pateikia Nano v3 mikrokontroleris, uždavinį sprendžia Nucleo 64 mikrokontroleris.

## Sujungimas

| Nano v3 | Nucleo 64 |
|---|---|
| 5V | 5V |
| GND | GND |
| D2 | PC6 |
| D10 | PA9 |

## Sprendimas

Uždaviniui spręsti buvo pasitelkti 2 laikrodžiai. TIM1 - signalui nuskaityti, TIM3 - signalui išsiųsti. Visų pirma, Nucleo gauna signalą per PA9. Gavus du signalus vieną po kito yra apskaičiuojamas signalo periodas. Padalinę TIM1 laikrodžio dažnį iš to periodo gauname gauto signalo dažnį Hercais. Gautas signalo dažnis būna apie 2% mažesnis nei realus.


Toliau yra atjungiamas singalo skaitymas. Vykdomas signalo dažnio pakeitimas pagal uždavinį. Apskaičiavus naują dažnį yra palaukiama 5 sekundes, kol Nano vėl galės priimti signalą. Tuomet per PC6 yra siunčiamas signalas pagal pakeistą dažnį 6 sekundes. Dabar vėl įjungiamas singalo skaitymas ir laukiama sekančio uždavinio signalo. 
