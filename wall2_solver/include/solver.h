#ifndef SOLVER_H
#define SOLVER_H

int solve_1(int frequency);
int solve_2(int frequency);
int solve_3(int frequency);
int solve_4(int frequency);
int solve_5(int frequency);
int solve_6(int frequency);

int solve(int frequency, int solver_level);

#endif