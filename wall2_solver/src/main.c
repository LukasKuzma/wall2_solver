#include <stdlib.h>
#include <string.h>
#include "stm32l4xx_hal.h"
#include "solver.h"

#define buffer_size 64

UART_HandleTypeDef UartHandle;
TIM_HandleTypeDef Tim1Handle;
TIM_HandleTypeDef Tim3Handle;
uint32_t base_frequency = 100000;
uint32_t capture_period = 0xFFFF;

volatile uint32_t IC1 = 0;
volatile uint32_t IC2 = 0;
volatile uint32_t Frequency = 0;

uint8_t tx_buffer[buffer_size];
uint8_t success_msg[buffer_size] = "done\r\n";
uint8_t waiting_msg[buffer_size] = "waiting\r\n";
volatile uint8_t is_captured = 0;
volatile uint8_t transmit_pulse = 0;

volatile int solver_level = 1;

void SystemClock_Config(void);
void GPIO_Init();
void TIM1_Init();
void TIM1_DeInit();
void TIM1_Start();
void TIM1_Stop();
void TIM3_Init();
void TIM3_DeInit();
void TIM3_Start(uint32_t frequency);
void TIM3_Stop();
void USART_Init();
void USART_DeInit();

int main(void){
    HAL_Init();
    GPIO_Init();
    TIM1_Init();
    TIM1_Start();
    TIM3_Init();
    USART_Init();
    while(1){
        if (transmit_pulse != 0){
            HAL_Delay(5000);
            memset(tx_buffer, '\0', buffer_size);
            itoa(solve(Frequency, solver_level), (char*) tx_buffer, 10);
            tx_buffer[strlen((char*) tx_buffer)] = '\r';
            tx_buffer[strlen((char*) tx_buffer)] = '\n';
            HAL_UART_Transmit(&UartHandle, tx_buffer, strlen((char*) tx_buffer), 1000);
            TIM3_Start(solve(Frequency, solver_level));
            HAL_Delay(6000);
            TIM3_Stop();
            transmit_pulse = 0;
            HAL_UART_Transmit(&UartHandle, success_msg, strlen((char*) success_msg), 1000);
            solver_level++;
            if (solver_level > 6){
                solver_level = 1;
            }
        }
        HAL_UART_Transmit(&UartHandle, waiting_msg, strlen((char*) waiting_msg), 1000);
    }
}

void SysTick_Handler(void)
{
    HAL_IncTick();
}

void TIM1_CC_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&Tim1Handle);
}

void TIM3_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&Tim3Handle);
}

void USART1_IRQHandler(void)
{
    HAL_UART_IRQHandler(&UartHandle);
}

void GPIO_Init(){
    GPIO_InitTypeDef GPIO_InitStruct;

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();

    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Pin = GPIO_PIN_8;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_9;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_6;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void USART_Init(){
    __HAL_RCC_USART1_CLK_ENABLE();

    UartHandle.Instance = USART1;
    UartHandle.Init.BaudRate = 9600;
    UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
    UartHandle.Init.StopBits = UART_STOPBITS_1;
    UartHandle.Init.Parity = UART_PARITY_NONE;
    UartHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    UartHandle.Init.Mode = UART_MODE_TX_RX;
    UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
    UartHandle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    HAL_UART_Init(&UartHandle);

    HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);

    __HAL_UART_ENABLE_IT(&UartHandle, UART_IT_RXNE);
}

void TIM1_Init()
{
    __HAL_RCC_TIM1_CLK_ENABLE();

    Tim1Handle.Instance = TIM1;
    Tim1Handle.Init.Period = capture_period;
    Tim1Handle.Init.Prescaler         = 0;
    Tim1Handle.Init.ClockDivision     = 0;
    Tim1Handle.Init.CounterMode       = TIM_COUNTERMODE_UP;
    Tim1Handle.Init.RepetitionCounter = 0;

    HAL_TIM_IC_Init(&Tim1Handle);

    TIM_IC_InitTypeDef sICConfig;
    sICConfig.ICPolarity  = TIM_ICPOLARITY_RISING;
    sICConfig.ICSelection = TIM_ICSELECTION_DIRECTTI;
    sICConfig.ICPrescaler = TIM_ICPSC_DIV1;
    sICConfig.ICFilter    = 0;   
    HAL_TIM_IC_ConfigChannel(&Tim1Handle, &sICConfig, TIM_CHANNEL_2);

    HAL_NVIC_SetPriority(TIM1_CC_IRQn, 0, 1);
    HAL_NVIC_EnableIRQ(TIM1_CC_IRQn);
}

void TIM1_Start(){
    HAL_TIM_IC_Start_IT(&Tim1Handle, TIM_CHANNEL_2);
}

void TIM1_Stop(){
    HAL_TIM_IC_Stop(&Tim1Handle, TIM_CHANNEL_2);
}

void TIM3_Init()
{
    __HAL_RCC_TIM3_CLK_ENABLE();
    Tim3Handle.Instance = TIM3;
    Tim3Handle.Init.Prescaler         = (SystemCoreClock / base_frequency) - 1;
    Tim3Handle.Init.ClockDivision     = 0;
    Tim3Handle.Init.CounterMode       = TIM_COUNTERMODE_UP;
    Tim3Handle.Init.RepetitionCounter = 0;

    TIM_OC_InitTypeDef sConfig;
    sConfig.OCMode       = TIM_OCMODE_PWM1;
    sConfig.OCPolarity   = TIM_OCPOLARITY_HIGH;
    sConfig.OCFastMode   = TIM_OCFAST_DISABLE;
    sConfig.OCNPolarity  = TIM_OCNPOLARITY_HIGH;
    sConfig.OCNIdleState = TIM_OCNIDLESTATE_RESET;
    sConfig.OCIdleState  = TIM_OCIDLESTATE_RESET;
    sConfig.Pulse = 1;
    HAL_TIM_PWM_ConfigChannel(&Tim3Handle, &sConfig, TIM_CHANNEL_1);
}

void TIM3_Start(uint32_t frequency){
    Tim3Handle.Init.Period = (base_frequency/frequency) - 1;
    HAL_TIM_PWM_Init(&Tim3Handle);
    HAL_TIM_PWM_Start(&Tim3Handle, TIM_CHANNEL_1);
}

void TIM3_Stop(){
    HAL_TIM_PWM_Stop(&Tim3Handle, TIM_CHANNEL_1);
}

void USART_DeInit(){
    __HAL_RCC_USART1_FORCE_RESET();
    __HAL_RCC_USART1_RELEASE_RESET();

    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9);
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_10);
}

void TIM1_DeInit(){
    __HAL_RCC_TIM1_FORCE_RESET();
    __HAL_RCC_TIM1_RELEASE_RESET();

    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_8);
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9);
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
    {
        if(is_captured == 0)
        {
            IC1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);
            is_captured = 1;
        }
        else if(is_captured == 1)
        {
            uint32_t Diff = 0;
            IC2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2); 
            is_captured = 0;
            if (IC2 > IC1)
            {
                Diff = (IC2 - IC1); 
            } else {
                return;
            } 
            Frequency = HAL_RCC_GetPCLK2Freq()/Diff;
            Frequency = (uint32_t)(1.02*Frequency);
            is_captured = 0;
            transmit_pulse = 1;
        }
    }
}
