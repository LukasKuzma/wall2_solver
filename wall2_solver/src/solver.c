#include <math.h>
#include "solver.h"

int solve_1(int frequency){
    return frequency;
}

int solve_2(int frequency){
    return frequency + 50;
}

int solve_3(int frequency){
    return (int)pow((double) frequency, 1.05);
}

int solve_4(int frequency){
    return (int)frequency/(M_PI_2);
}

int solve_5(int frequency){
    return (int)frequency >> 1;
}

int solve_6(int frequency){
    return (int)frequency/(((1 + pow(5, 0.5)) / 2) / 2);
}

int solve(int frequency, int solver_level){
    int result = 0;
    switch(solver_level){
        case 1:
            result = solve_1(frequency);
            break;
        case 2:
            result = solve_2(frequency);
            break;
        case 3:
            result = solve_3(frequency);
            break;
        case 4:
            result = solve_4(frequency);
            break;
        case 5:
            result = solve_5(frequency);
            break;
        case 6:
            result = solve_6(frequency);
            break;
    }
    return result;
}
